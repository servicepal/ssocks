pwd := $(shell pwd)
ver := 1.0

.PHONY:
all: clean run

.PHONY:
clean: stop
	@docker rm ssocks
	@-docker rmi $(shell docker images --filter "dangling=true" -q --no-trunc)

.PHONY:
stop:
	@docker stop ssocks

.PHONY:
run:
	@docker run --name ssocks \
		-p 4040:4040 \
		--restart=unless-stopped \
		-m 512M --memory-swap -1 \
		--cpuset-cpus="0,1" \
		-d ssocks:$(ver)

.PHONY:
build:
	@docker build -f Dockerfile -t ssocks:$(ver) .

