FROM python:2.7-onbuild

USER root

# install code
RUN mkdir -p /opt/code
WORKDIR /opt/code
ADD . /opt/code

EXPOSE 4040 

CMD ["ssserver", "-c", "/opt/code/ssocks.json", "--user", "nobody", "-d", "start"]
